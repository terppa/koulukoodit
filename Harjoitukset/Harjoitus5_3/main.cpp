#include <iostream>
#include <fstream>
#include <string>
#include <map>

int main() {

	std::map<std::string,int> words;
	std::ifstream fileInput;
	std::string word;
	fileInput.open("sanoja.txt");
	while (std::getline(fileInput, word, ' ')) {
		if (words.find(word) == words.end()) {
			words[word] = 1;
		}
		else {
			words[word]++;
		}
	}
	for (auto i = words.begin(); i != words.end();i++) {
		std::cout << i->first << " esiintyy: " << i->second << " kertaa." << std::endl;
	}
	fileInput.close();
	system("pause");
	return 0;
}