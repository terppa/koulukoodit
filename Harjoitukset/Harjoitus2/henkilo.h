#pragma once
#ifndef HENKILO_H
#define HENKILO_H
#include <string>
#include <iostream>
using std::string;

class Henkilo
{
public:
	Henkilo();
	Henkilo(const string&,const string&, const string&);
	Henkilo(string);
	virtual ~Henkilo();
	void setFirstName(string fname) const;
	void setLastName(string lname);
	void setSocialCode(string scode);
	string getFirstName() const;
	string getLastName()const;
	string getSocialCode()const;
	void printInformation() const;
	Henkilo& operator= (const string fullName);
	Henkilo& operator= (const Henkilo& old);
	operator string();

private:
	mutable string firstname_;
	string lastname_;
	string socialcode_;
	string iCustomer_;
};
class Henkilo_ptr
{
public:
	Henkilo_ptr();
	Henkilo_ptr(string fname,string lname,string scode);
	~Henkilo_ptr();
	Henkilo &operator*()const;
	Henkilo *operator->()const;
private:
	void Load()const;
	string iCustomer;
	mutable Henkilo *iS; //Dynamic resource
};

#endif
