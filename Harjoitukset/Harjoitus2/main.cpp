#include <iostream>
#include "henkilo.h"
using std::cout;
using std::cin;
using std::endl;
int main()
{
	while (true)
	{

		Henkilo_ptr Customer1("Donald", "Duck", "123123"); //smart-osoitin
		Henkilo_ptr Customer2("Hessu", "Hopo", "321321");
		Henkilo_ptr Customer3("Tauno", "Palo", "123");
		string sotu = "";
		bool isFound = false;
		cout << "Anna sotu jota haetaan: " << endl;
		cout << "Voit poistua ohjelmasta painamalla p" << endl;
		cin >> sotu;
		Henkilo_ptr pointers[3] = { Customer1,Customer2,Customer3 };
		if (sotu == "p" || sotu == "P") {
			break;
		}
		for (int i = 0; i < sizeof(pointers) / sizeof(pointers[0]); i++) {
			if (sotu == pointers[i]->getSocialCode()) {
				pointers[i]->printInformation();
			}
		}
		if (!isFound) {
			cout << "Sotua ei ole olemassa \n\n";
		}

	}
	return 0;
}