#include <cstdlib>
#include <iostream>

#include <string>
using namespace std;

class City {
private:
	class Data
	{
	public:
		Data(string paikkakunta = "") : iName(paikkakunta), iCount(1)
		{
		}
		const string &Name(void) const
		{
			return iName;
		}
		long Count()
		{
			return iCount;
		}
		void operator++()
		{
			iCount++;
		}
		long operator--()
		{
			iCount--;
			return iCount;
		}
		void inc() {
			iCount++;
		}
		void dec() {
			iCount--;
		}
	private:
		string iName;
		long iCount;
	};

	Data* iData;

public:
	City()
	{
		iData = new Data;
	}
	City(const string &name)
	{
		iData = new Data(name);
	}
	City(City &city)
	{
		iData = new Data(city.Name());
		City* c = &city;
		c->iData->inc();
		if (iData->Count() != 0) {
			iData->dec();
		}
	}
	~City()
	{
		this->iData->dec();// one reference less


	}
	City &operator=(City &city)
	{
		if (this == &city) {
			return *this;
		}
		City* c = &city;
		c->iData->inc();
		
		iData = new Data(city.Name());
		if (iData->Count() != 0) {
			iData->dec();
		}
		// left side data one less
		int i = 0;

		// right side data one more


		return *this;
	}
	const string &Name() const
	{
		return iData->Name();
	}
	const long &Count() const{
		return iData->Count();
	}
	
};

int main()
{
	City c1("TAMPERE");//creation
	City c2(c1);//copy constructor
	City c3; //default constructor
	City c4;
	c4 = c2;
	cout << "Assignment" << endl;
	c3 = c1;
	cout << "C1: Nimi: " << c1.Name() << ", Referenssi lukema: " << c1.Count() << endl;
	cout << "C2: Nimi: " << c2.Name() << ", Referenssi lukema: " << c2.Count() <<endl;
	cout << "C3: Nimi: " << c3.Name() << ", Referenssi lukema: " << c3.Count()<< endl;
	cout << "C4: Nimi: " << c4.Name() << ", Referenssi lukema: " << c4.Count()<< endl;
	system("PAUSE");
	return EXIT_SUCCESS;
}


