// Stack class template test program. Function main uses a
// function template to manipulate objects of type Stack< T >.
#include <iostream>
using std::cout;
using std::cin;
using std::endl;
#include "tstack1.h"
#include "Murtoluku.h"// Stack class template definition
// function template to manipulate Stack< T >
template< typename T ,int SIZE>
void testStack
(
	Stack< T,SIZE > &theStack, // reference to Stack< T >
	T value, // initial value to push
	T increment, // increment for subsequent values
	const char *stackName) // name of the Stack < T > object
{
	cout << "\nPushing elements onto " << stackName << '\n';
	while (theStack.push(value))
	{
		cout << value << ' ';
		value += increment;
	} // end while
	cout << "\nStack is full. Cannot push " << value
		<< "\n\nPopping elements from " << stackName << '\n';
	while (theStack.pop(value))
		cout << value << ' ';
	cout << "\nStack is empty. Cannot pop\n";
} // end function testStack
int main()
{
	std::cout << "Tehtava 1&2 -------- Stack --------" << std::endl;
	Murtoluku murto(3, 4);
	Murtoluku inc(2, 4);
	Stack< double > doubleStack;
	Stack< int > intStack;
	Stack<float> floatStack;
	Stack <Murtoluku> murtoStack;
	testStack(doubleStack, 1.1, 1.1, "doubleStack");
	testStack(intStack, 1, 1, "intStack");
	testStack(murtoStack, murto, inc, "murtoStack");
	
	std::cout << std::endl << "TEHTAVA 3 --- omaTekstiPino tulostaa stringin stringStackista ------" << std::endl;
	
	omaTekstiPino < std::string> oma;
	oma.push("Testi");
	oma.push("Joku");
	oma.push("Juu");
	std::cout<< oma;
	
	std::cout << std::endl << "TEHTAVA 4 ---- omaPino kertoo paljonko on vapaata tilaa ----" << std::endl;
	omaPino<std::string> p;
	p.push("Testi");
	std::cout <<"Lisayksien jalkeen tilaa pinossa on: " <<p.space() << std::endl;
	system("pause");
	return 0;
}