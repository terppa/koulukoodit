#pragma once
#include<iostream>
#include <string>
#ifndef TSTACK1_H
#define TSTACK1_H
template< typename T, int SIZE = 10>
class Stack
{
public:
	Stack(); // default constructor (stack size 10)
				 // destructor
	~Stack()
	{
		delete[] stackPtr;
	} // end ~Stack destructor
	bool push(const T&); // push an element onto the stack
	bool pop(T&); // pop an element off the stack
				  // determine whether Stack is empty
	bool isEmpty() const
	{
		return top == -1;
	} // end function isEmpty
	  // determine whether Stack is full
	bool isFull() const
	{
		return top == size - 1;
	} // end function isFull
	T getStackElement(int);
	int getTop();
private:
	int size; // # of elements in the stack
	int top; // location of the top element
	T *stackPtr; // pointer to the stack
}; // end class Stack
   // constructor
template< typename T, int SIZE >

Stack< T, SIZE >::Stack()
{
	size = SIZE;
	top = -1; // Stack initially empty
	stackPtr = new T[SIZE]; // allocate memory for elements
} // end Stack constructor
  // push element onto stack;
  // if successful, return true; otherwise, return false
template< typename T, int SIZE >
bool Stack< T, SIZE >::push(const T &pushValue)
{
	if (!isFull())
	{
		stackPtr[++top] = pushValue; // place item on Stack
		return true; // push successful
	} // end if
	return false; // push unsuccessful
} // end function push
  // pop element off stack;
  // if successful, return true; otherwise, return false
template< typename T, int SIZE>
bool Stack< T, SIZE >::pop(T &popValue)
{
	if (!isEmpty())
	{
		popValue = stackPtr[top--]; // remove item from Stack
		return true; // pop successful
	} // end if
	return false; // pop unsuccessful
} // end function pop

template<typename T, int SIZE>
T Stack<T, SIZE>::getStackElement(int index) {
	return stackPtr[index];
}

template<typename T, int SIZE>
inline int Stack<T, SIZE>::getTop()
{
	return top + 1;
}

template<typename T, int SIZE=10>
class omaTekstiPino :public Stack<T, SIZE>
{
public:
	omaTekstiPino();
	omaTekstiPino(Stack&, int);
	~omaTekstiPino();
	friend std::ostream& operator<<(std::ostream &os,  omaTekstiPino &o) {
		 omaTekstiPino*a = &o;
		if (a->getStackElement(0) != "") {

			a->tuloste_ =  a->getStackElement(1);
		}
		else a->tuloste_ = "tyhja";
		return os << "Alkiossa 1/"<<a->getTop()<<" on string: " << a->tuloste_ << std::endl;
	}
	void print();
private:
	std::string tuloste_;
};

template<typename T, int SIZE>
omaTekstiPino<T, SIZE>::omaTekstiPino()
{
}


template<typename T, int SIZE>
omaTekstiPino<T, SIZE>::~omaTekstiPino()
{
}

template<typename T, int SIZE>
void omaTekstiPino<T, SIZE>::print()
{
	std::cout << tuloste_ << std::endl;
}


template<typename T, int SIZE=10>
class omaPino : public Stack<T, SIZE>
{
public:
	omaPino();
	~omaPino();
	int space() {
		return spaceLeft_ - getTop();
	}
private:
	int spaceLeft_;
};

#endif

template<typename T, int SIZE>
omaPino<T, SIZE>::omaPino():spaceLeft_(SIZE)
{
}

template<typename T, int SIZE>
omaPino<T, SIZE>::~omaPino()
{
}
