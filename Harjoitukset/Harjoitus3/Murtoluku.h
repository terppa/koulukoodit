#pragma once
#include <iostream>
class Murtoluku
{
public:
	Murtoluku();
	Murtoluku(int o, int n);
	~Murtoluku();
	Murtoluku& operator+ (Murtoluku&);
	Murtoluku& operator+= (Murtoluku&);
	friend std::ostream& operator<<(std::ostream &os, Murtoluku const &m);
	void naytaMurtoluku();
private:
	int osoittaja_;
	int nimittaja_;
};

