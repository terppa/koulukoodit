#pragma once
#include "Henkilo.h"
class Opettaja :
	public Henkilo
{
public:
	Opettaja();
	virtual ~Opettaja();
	Opettaja(const string&, const string&, const string&, string);
private:
	string osaamiskeskus_;

};

