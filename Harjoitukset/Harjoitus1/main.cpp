#include <iostream>
#include "Henkilo.h"
#include "Opettaja.h"
#include "Oppilas.h"
using std::cout; using std::endl;
int main() {
	//Harjoitus 1 teht�v� 1&2
	Henkilo h1;
	h1 = "Juice Leskinen";
	const Henkilo h2("Matti", "Meikalainen", "123456-789");
	string nimi = h1;
	string k = h1;
	
	cout << "TEHTAVA 1&2" <<endl;
	cout << "Henkilo muutettuna stringiksi " + nimi << endl;
	cout << "Henkilo luotuna stringista: " + h1.getFirstName() + " " + h1.getLastName() << endl;
	cout << "Etunimi ennen vaihtoa: " + h2.getFirstName() + " " + h2.getLastName() << endl;
	h2.setFirstName("Maija");
	cout << "Etunimen vaihto: " + h2.getFirstName() + " " + h2.getLastName() << endl<<endl<<endl;
	
	//-----------------Harjoitus1 teht�v� 3-------------------------//
	std::cout << "TEHTAVA 3"<<endl;
	Henkilo * ope1 = new Opettaja("Oppi", "Opettaja", "123", "Ohjelmointi");
	Henkilo * ope2 = new Opettaja("Essi", "Esimerkki", "321", "Elektroniikka");
	Henkilo * opp1 = new Oppilas("Maija", "Mallintaja", "231", "00030");
	Henkilo * opp2 = new Oppilas("Jussi", "Joutava", "112", "020103");
	Henkilo * opp3 = new Oppilas("Jaakko", "Jankuttaja", "112", "020103");
	Henkilo* taulu[5] = { ope1,ope2,opp1,opp2,opp3 };
	int count = 0;
	for each(Henkilo* ptr in taulu) {
		if (dynamic_cast<Oppilas*>(ptr)) {
			count++;
		}

	}
	std::cout << "Oppilaita taulussa on: " << count << "/"<< sizeof(taulu)/sizeof(taulu[0]) <<endl;
	system("pause");
	return 0;
	
}