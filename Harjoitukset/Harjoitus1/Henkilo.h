#pragma once
#include <string>
using std::string;
class Henkilo
{
public:
	Henkilo();
	Henkilo(string fname, string lname, string scode);
	virtual ~Henkilo();
	void setFirstName(string fname) const;
	void setLastName(string lname);
	void setSocialCode(string scode);
	string getFirstName() const;
	string getLastName()const;
	string getSocialCode()const;
	Henkilo& operator= (const string fullName);
	Henkilo& operator= (const Henkilo& old);
	operator string();

private:
	mutable string firstname_;
	string lastname_;
	string socialcode_;
};
