#include "Henkilo.h"



Henkilo::Henkilo() :firstname_(""), lastname_(""), socialcode_("")
{
}

Henkilo::Henkilo(string fname, string lname, string scode) : firstname_(fname), lastname_(lname), socialcode_(scode)
{
}

Henkilo::~Henkilo()
{
}

void Henkilo::setFirstName(string fname) const
{
	firstname_ = fname;
}

void Henkilo::setLastName(string lname)
{
	lastname_ = lname;
}

void Henkilo::setSocialCode(string scode)
{
	socialcode_ = scode;
}

string Henkilo::getFirstName() const
{
	return firstname_;
}

string Henkilo::getLastName() const
{
	return lastname_;
}

string Henkilo::getSocialCode() const
{
	return socialcode_;
}

Henkilo & Henkilo::operator=(const string fullName)
{
	if (fullName != "") {
		string delimiter = " ";
		this->firstname_ = fullName.substr(0, fullName.find(delimiter));
		this->lastname_ = fullName.substr(fullName.find(delimiter) + 1, fullName.length());
	}
	return *this;
}
Henkilo & Henkilo::operator=(const Henkilo& old)
{
	if (this != &old) {
		string delimiter = " ";
		this->firstname_ = old.firstname_;
		this->lastname_ = old.lastname_;
		this->socialcode_ = old.socialcode_;
	}
	return *this;
}
Henkilo::operator string() {
	return firstname_ + " " + lastname_;
}
