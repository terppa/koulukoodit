#include <iostream>
#include <fstream>
#include <string>
#include <set>

int main() {

	std::multiset<std::string> words;
	std::set<std::string> checkedWords;
	std::ifstream fileInput;
	std::string word;
	fileInput.open("sanoja.txt");
	while (std::getline(fileInput, word, ' ')) {

		words.insert(word);

	}
	for (std::string s : words) {
		if (checkedWords.find(s) == checkedWords.end()) {
			checkedWords.insert(s);
			std::cout << "'" + s + "'" << " esiintyy tekstissa: " << words.count(s) << " kertaa" << std::endl;
		}
	}
	fileInput.close();
	system("pause");
	return 0;
}