#pragma once
#include "tstack1.h"
#include <string>
#include <iostream>
template<typename T,int SIZE>
class omaTekstiPino :public Stack<T,SIZE>
{
public:
	omaTekstiPino();
	omaTekstiPino(Stack&);
	~omaTekstiPino();
	void print();
private:
	std::string tuloste_;
	Stack pino_;
};


