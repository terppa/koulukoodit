#include <iostream>
#include "Kauppa.h"
#include <fstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <utility>
#include <map>
int main() {
	int counter = 0;
	auto l = [&counter]() {counter++;}; //Lambda jolla laskuria kasvatetaan
	
	std::string poisto;
	std::vector<Kauppa> V;
	std::multimap<std::string, Kauppa> map;
	std::set<std::string> myyjat;
	std::ifstream tied("nokia18032009.txt");
	getline(tied, poisto);
	std::copy(std::istream_iterator<Kauppa>(tied), std::istream_iterator<Kauppa>(), back_inserter(V));

	for (Kauppa k : V) {
		map.insert(std::pair<std::string, Kauppa>(k.getMyyja(), k));
		myyjat.insert(k.getMyyja());
	}

	for (std::string i : myyjat) {
		counter = 0;
		auto low = map.lower_bound(i);
		auto up = map.upper_bound(i);
		for (auto it = low;it != up;++it) {
			std::string ostaja = (*it).second.getOstaja();
			std::string myyja = (*it).first;
			if (ostaja == myyja) {
				l();
			}
		}
		std::cout << "Myyjan " << i << " kaupat : " << counter << std::endl;
	}
	system("pause");
	return 0;
}