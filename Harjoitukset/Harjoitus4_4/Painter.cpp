#include "Painter.h"

int Painter::Count = 0;
Painter* Painter::single = NULL;

Painter::Painter()
{
}



Painter * Painter::Create()
{
	std::cout << "Creating Object.. " << std::endl;
	if (Count < 5) {
		Count++;
		single = new Painter();
		std::cout << "Object count: " << Count << std::endl << std::endl;
		return single;
	}
	else {
		std::cout << "You can only create 5 objects!" << std::endl << std::endl;
		return NULL;
	}
	
}

void Painter::method()
{
	std::cout << "Testing object" << std::endl;
}

Painter::~Painter()
{
	Count--;
	std::cout << "Deleting Object.."<<std::endl << "Object count: " << Count << std::endl <<std::endl;
}
