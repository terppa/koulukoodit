#include "Painter.h"
#include <iostream>

int main() {

	Painter *p1 = Painter::Create();
	Painter *p2 = Painter::Create();
	Painter *p3 = Painter::Create();
	Painter *p4 = Painter::Create();
	Painter *p5 = Painter::Create();
	delete p1;
	Painter *p6 = Painter::Create();
	Painter *p7 = Painter::Create();
	delete p2;
	delete p3; 
	delete p4; 
	delete p5; 
	delete p6; 
	delete p7;
	system("pause");
	return 0;
}