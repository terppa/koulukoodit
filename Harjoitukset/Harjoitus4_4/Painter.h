#pragma once
#include <iostream>
class Painter
{
public:
	//Painter();
	static Painter* Create();
	void method();
	~Painter();
private:
	static int Count;
	static Painter * single;
	Painter();
};

