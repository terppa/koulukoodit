#include "Kauppa.h"

Kauppa::Kauppa()
{
}

Kauppa::Kauppa(string kello, double hinta, int maara, string ostaja, string myyja) :aika_(kello), hinta_(hinta), maara_(maara), ostaja_(ostaja), myyja_(myyja)
{
}

Kauppa::Kauppa(int maara) : maara_(maara)
{
}

Kauppa::~Kauppa()
{
}

bool Kauppa::operator<(const Kauppa & k) const
{
	return this->hinta_ > k.hinta_;
}

bool Kauppa::operator==(const Kauppa & k) const
{
	return this->maara_ == k.maara_;
}


ostream & operator<<(ostream & out, const Kauppa & k)
{
	return out << "Kello: " << k.aika_ << std::endl << "Myyja: " << k.myyja_ << std::endl << "Hinta: " << k.hinta_ << std::endl << "Ostaja: " << k.ostaja_ << std::endl << "M��r�: " << k.maara_ << std::endl;
}

istream & operator>>(istream & in, Kauppa & k)
{
	return in >> k.aika_ >> k.hinta_ >> k.maara_ >> k.myyja_ >> k.ostaja_;
}
