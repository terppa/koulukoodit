#include <iostream>
#include "Kauppa.h"
#include <fstream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <utility>
#include <map>
int main() {
	int counter = 0;
	int maara = 0;
	int summa = 0;
	auto l = [&counter]() {counter++;}; //Lambda jolla laskuria kasvatetaan
	auto la = [&maara](auto a) {maara += a;};//Lambda jolla lasketaan myytyjen osakkeiden m��r�
	auto lam = [&summa](auto a, auto b) {summa += a*b;};//Lambda jolla lasketaan myytyjen osakkeiden hintojen summa
	std::string myyja;
	std::getline(std::cin, myyja);
	std::string poisto;
	std::vector<Kauppa> V;
	std::multimap<std::string, Kauppa> map;

	std::ifstream tied("nokia18032009.txt");
	getline(tied, poisto);
	std::copy(std::istream_iterator<Kauppa>(tied), std::istream_iterator<Kauppa>(), back_inserter(V));

	for (Kauppa k : V) {
		map.insert(std::pair<std::string, Kauppa>(k.getMyyja(), k));
	}

	if (map.find(myyja) == map.end()) {
		std::cout << "Myyjaa ei ole olemassa tai se on virheellisesti syotetty" << std::endl;
	}
	else {
		auto low = map.lower_bound(myyja);
		auto up = map.upper_bound(myyja);
		for (auto it = low;it != up;++it) {
			int maara = it->second.getMaara();
			double hinta = it->second.getHinta();
			l();
			la(maara);
			lam(hinta, maara);
		}
		std::cout << "Myyjan " << myyja << " kauppojen maara : " << counter << " myydyt osakkeet: " << maara << " ja yhteensa: " << summa << std::endl;
	}

	system("pause");
	return 0;
}