#include "Maksimi.h"
#include <iostream>
int main() {

	Maksimi m;

	std::cout << "MAX(int) luvuista 3 ja 43 on: " << m.max(3, 43) << std::endl;
	std::cout << "MAX(float) luvuista 6.2 ja 4.3 on: " << m.max(6.2f, 4.3f) << std::endl;
	std::cout << "MAX(string) stringeista Pekka ja Antti on: " << m.max("Pekka", "Antti") <<std::endl;

	system("pause");
	return 0;
}