#pragma once
#include <string>
class Maksimi
{
public:
	Maksimi();
	~Maksimi();
	int max(const int eka, const int toka);
	float max(const float eka, const float toka);
	const std::string & max(const std::string & eka, const std::string & toka);
};

