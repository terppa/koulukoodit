#include "Maksimi.h"



Maksimi::Maksimi()
{
}


Maksimi::~Maksimi()
{
}

int Maksimi::max(const int eka, const int toka)
{
	if (eka > toka) {
		return eka;
	}
	return toka;
}

float Maksimi::max(const float eka, const float toka)
{
	if (eka > toka) {
		return eka;
	}
	return toka;
}

const std::string & Maksimi::max(const std::string & eka, const std::string & toka)
{
	if (eka > toka) {
		return eka;
	}
	return toka;
}
