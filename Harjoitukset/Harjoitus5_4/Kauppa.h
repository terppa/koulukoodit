#pragma once
#ifndef KAUPPA_H
#define KAUPPA_H
#include <string>
using std::string;
#include <iostream>
#include <set>
using std::ostream;
using std::istream;
class Kauppa
{
public:
	Kauppa();
	Kauppa(string kello, double hinta, int maara, string ostaja, string myyja);
	void setSyote(std::string syote);
	~Kauppa();
	std::string getMyyja() {
		return myyja_;
	}
	double getHinta() {
		return hinta_;
	}
	int getMaara() {
		return maara_;
	}

	friend ostream & operator<<(ostream & out, const Kauppa &k);
	friend istream & operator>>(istream & in, Kauppa &k);
	bool operator <(const Kauppa & k) const;
	inline void Kauppa::operator()(const Kauppa k) const {

		if (k.myyja_ == syote_) {
			Count++;
			summa += k.maara_ * k.hinta_;
		}

	}
	int getCount();
	int getSumma();
private:
	string aika_;
	double hinta_;
	int maara_;
	string ostaja_;
	string myyja_;
	std::string syote_;
	static int summa;
	static int Count;

	
};
#endif

