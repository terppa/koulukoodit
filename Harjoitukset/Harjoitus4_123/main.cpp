#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <algorithm>
#include <cctype>
bool ispunc(const char& c) {
	return std::ispunct(static_cast<int>(c));
}
int main() {
	std::ifstream fileInput;
	std::ofstream fileOutput;
	std::string line;
	std::string word;
	//TEHT�V� 1
	std::cout << "Tehtavan 1 toteutus" << std::endl;
	std::vector<std::string> teht1Vector;
	fileInput.open("teht1.txt");
	if (fileInput.is_open()) {
		while (std::getline(fileInput, word, ' ')) {
			std::cout << "Adding value to vector: " << word << std::endl;
			teht1Vector.push_back(word);
		}
	}
	fileInput.close();
	std::sort(teht1Vector.begin(), teht1Vector.end());
	std::cout<< std::endl << "Tehtava 1 lopputulos" << std::endl;
	fileOutput.open("teht1Output.txt");
	for (int i = 0; i < teht1Vector.size(); i++) {
		std::cout << "Aakkosjarjestyksessa: " << teht1Vector.at(i) << std::endl;
		fileOutput << teht1Vector.at(i) + "\n";
	}
	fileOutput.close();
	//TEHT�V� 2
	std::cout << std::endl << "Tehtavan 2&3 lisaykset" <<std::endl;
	std::vector<std::string> teht2Vector;
	std::vector<std::string> teht3Vector;
	fileInput.open("merkkijono.txt");
	if (fileInput.is_open()) {
		while (std::getline(fileInput, line)) {
			std::replace_if(line.begin(), line.end(), ispunc, ' ');
			std::istringstream iss(line);
			while (iss >> word) {
				if (word.length() > 2) {
					std::cout << "Adding value to teht3vector: " << word << std::endl;
					teht3Vector.push_back(word);
				}
				std::cout << "Adding value to teht2vector: " << word << std::endl;
				teht2Vector.push_back(word);
			}
			
		}
	}
	fileInput.close();
	std::sort(teht2Vector.begin(), teht2Vector.end());
	std::cout << std::endl << "Tehtavan 2 lopputulos" << std::endl;
	fileOutput.open("merkkijono_abc.txt.");
	for (int i = 0; i < teht2Vector.size(); i++) {
		std::cout << "Aakkosjarjestyksessa: " << teht2Vector.at(i) << std::endl;
		fileOutput << teht2Vector.at(i) + "\n";
	}
	fileOutput.close();
	std::sort(teht3Vector.begin(), teht3Vector.end());
	std::cout << std::endl << "Tehtavan 3 lopputulos" << std::endl;
	fileOutput.open("merkkijono_abc_2.txt.");
	for (int i = 0; i < teht3Vector.size(); i++) {
		std::cout << "Aakkosjarjestyksessa: " << teht3Vector.at(i) << std::endl;
		fileOutput << teht3Vector.at(i) + "\n";
	}
	fileOutput.close();
	
	

	system("pause");
	return 0;
}