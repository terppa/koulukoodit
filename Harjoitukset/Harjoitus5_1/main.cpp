#include <set>
#include <iostream>
#include <string>
int main() {
	std::string input;
	std::getline(std::cin, input);
	std::set<char> chars;
	for (char c : input) {
		chars.insert(c);
	}
	
	for (char c : chars) {
		std::cout << c << std::endl;
	}
	system("pause");
	return 0;
}